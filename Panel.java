import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class Panel extends JPanel implements MouseMotionListener, MouseListener {

	private World world;
	private Person[] people;
	private boolean running;
	private boolean userHasAids;
	private boolean[] userRisk;
	private int womanWithMaxAttractiveness;
	private long timeUntilSTD;
	private ClockLabel l;
	private int stepCnt;
	boolean[][] menRisk;
	int[] attractiveness;
	// Constructor to set the size of the world and set up the panel.

	public Panel(int n, int maxAttractiveness, boolean[][] menRisk, int[] attractiveness) {
		world = new World(n);
		userHasAids = false;
		this.userRisk = menRisk[0];
		l = new ClockLabel();
		this.menRisk = menRisk;
		this.attractiveness = attractiveness;
		add(l);
		makeNewPeople(200);
		setBackground( Color.white );
		setBorder( new EtchedBorder() );
		addMouseListener(this);
		addMouseMotionListener(this);
		setCursor( new Cursor(Cursor.CROSSHAIR_CURSOR) );
		womanWithMaxAttractiveness = maxAttractiveness;
		stepCnt = 0;
		
	}

	// Method to recreate all new people

	public void makeNewPeople(int n) {
		people = new Person[n];
		boolean boy = true;
		for ( int i=0; i<n; i++ ) {
			Person person;
			if (i<100){
				boy = false;
				person = new Person(world,boy,attractiveness[i], i, menRisk[0]);
			}else{ 
				boy = true;
				person = new Person(world,boy,7, i, menRisk[i-100]);				
			}
			
			double random =  Math.random();
			if ( random < 0.05 ) {
				//Diseased people
				person.setColor(Color.YELLOW);
			}
			if ( random < 0.03 ) {
				//Diseased people
				person.setColor(Color.green);
			}
			if ( random < 0.01 ) {
				//Diseased people
				person.setColor(Color.red);
			}

			people[i] = person;
		}

		Person person = new Person(world, true, 0,100,userRisk); //The user is going to be a boy for this experiment
		person.setColor(Color.BLUE);
		people[100] = person;
		people[50].setColor(Color.red); //Just to make sure that I get at least one person with STD
	}

	// Method to tell the mouse listener that the thread is running.

	public void setRunning( boolean t ) {
		makeNewPeople(people.length);
		running = t;
		userHasAids = false;

		if ( running ) {
			setCursor( new Cursor(Cursor.DEFAULT_CURSOR) );
			setTimeUntilSTD(System.currentTimeMillis());
		} else {

			setCursor( new Cursor(Cursor.CROSSHAIR_CURSOR) );
		}
	}


	// Paint the grid.
	public void paint(Graphics g) {

		super.paint(g);

		if (people[100].getColor() == Color.RED && userHasAids == false){
			userHasAids = true;
			//JOptionPane.showMessageDialog(this, "You just caught a sexually transmitted disease...");
			setTimeUntilSTD(System.currentTimeMillis() - timeUntilSTD);
			System.out.println(" HEERE " +timeUntilSTD);
		}
		// Paint the people
		g.setColor( Color.green );
		for ( int i=0; i<people.length; i++ ) {
			Person person = people[i];
			g.setColor(person.getColor());
			if (person.getAttractiveness() == womanWithMaxAttractiveness || person.getAttractiveness() == womanWithMaxAttractiveness-1){
				g.fillOval(5*person.getX()-5,5*person.getY()-5,10,10);
			}
			else
				g.fill3DRect(5*person.getX()-5,5*person.getY()-5,10,10, true);
		}

		g.setColor (people[100].getColor());
		g.fillOval(5*people[100].getX()-5,5*people[100].getY()-5,20,20);
	} 


	public void step(boolean t) {
		for ( int i=0; i<people.length; i++ ) {
			people[i].step(t);
		}

		Person[][] position = new Person[world.size()][world.size()];

		for ( int i=0; i<people.length; i++ )   {

			int X = people[i].getX();
			int Y = people[i].getY();

			if(position[X][Y] == null)
				position[X][Y] = people[i];
			else{
				Color firstCol = people[i].getColor();
				if (people[i].getColor() != Color.blue  && people[i].getColor() != Color.gray  && 
						people[i].getBoy() && !position[X][Y].getBoy() && people[i].risk[position[X][Y].getID()]){

					if (position[X][Y].getColor() == Color.YELLOW || position[X][Y].getColor() == Color.RED || 
							position[X][Y].getColor() == Color.GREEN || position[X][Y].getColor() == Color.black){
						if (Math.random() < 0.2){
							position[X][Y].setColor(Color.black);
							position[X][Y].listOfContacts.add(people[i]);
						}						
						
					}
					else {
						if (firstCol == Color.YELLOW){
							if (Math.random() < 0.9){
								position[X][Y].setColor(firstCol);
								position[X][Y].listOfContacts.add(people[i]);
							}
						}
						if (firstCol == Color.GREEN){
							if (Math.random() < 0.5){
								position[X][Y].setColor(firstCol);
								position[X][Y].listOfContacts.add(people[i]);
							}
						}
						if (firstCol == Color.RED){
							if (Math.random() < 0.4){
								position[X][Y].setColor(firstCol);
								position[X][Y].listOfContacts.add(people[i]);
							}
						}
					}
					
				}
				Color sndCol = position[X][Y].getColor();
				if (position[X][Y].getColor() != Color.blue && position[X][Y].getColor() != Color.gray    &&
						position[X][Y].getBoy() && !people[i].getBoy() && position[X][Y].risk[people[i].getID()]){

					if (people[i].getColor() == Color.YELLOW || people[i].getColor() == Color.RED || 
							people[i].getColor() == Color.GREEN || people[i].getColor() == Color.black){
						if (Math.random() < 0.2){
							people[i].setColor(Color.black);
							people[i].listOfContacts.add(position[X][Y]);
						}
						
					}
					else {
						if (sndCol == Color.YELLOW){
							if (Math.random() < 0.9){
								people[i].setColor(sndCol);
								people[i].listOfContacts.add(position[X][Y]);
							}
						}
						if (sndCol == Color.GREEN){
							if (Math.random() < 0.5){
								people[i].setColor(sndCol);
								people[i].listOfContacts.add(position[X][Y]);
							}
						}
						if (sndCol == Color.RED){
							if (Math.random() < 0.4){
								people[i].setColor(sndCol);
								people[i].listOfContacts.add(position[X][Y]);
							}
						}
					}
				}
			}
		}

		l.update((stepCnt++)/50);
		repaint();
	}

	public Person[] getPeople(){
		return people;
	}

	// Mouse listener methods to control drawing on the world.

	public void mouseClicked( MouseEvent e ) {
	}

	public void mousePressed( MouseEvent e ) {
		if ( !running ) {
			repaint();
		}
	}

	public void mouseReleased( MouseEvent e ) {
	}

	public void mouseEntered( MouseEvent e ) {
	}

	public void mouseExited( MouseEvent e ) {
	}

	public void mouseDragged( MouseEvent e ) {
		if ( !running ) {
			repaint();
		}
	}

	public void mouseMoved( MouseEvent e ) {
	}

	public long getTimeUntilSTD() {
		return timeUntilSTD;
	}

	public void setTimeUntilSTD(long timeUntilSTD) {
		this.timeUntilSTD = timeUntilSTD;
	}

	public void resetClockLabel(){
		stepCnt = 0;
	}
	
	class ClockLabel extends JLabel{
		
		public void update(int stepCnt) {
			setText("");
			//setText( (new Date()).getSeconds()- date + " years");
			setText("What happened after " + stepCnt + " years...");
		}
	}

}