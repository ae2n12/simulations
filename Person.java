import java.awt.Color;
import java.util.ArrayList;

public class Person {
	
	private int id;
	private World world;			// Reference to the world
	private int x;					// x position
	private int y;					// y postition
	private int facing;				// Direction facing
	private Color color;            //Color of the person
	private int attractiveness;    
	private boolean boy;           // Is it a boy?
	public boolean[] risk = new boolean[100];         // table which shows with which of the stimuli the boy will be risky
	public ArrayList<Person> listOfContacts;
	public ArrayList<Person> listOfAllContacts;
	
	public Person( World w, boolean boy, int attractiveness, int id, boolean[] risk) {
		this.id = id;
		world=w;
		y= w.size()/2;
		x=w.size()/2;
		facing = (int)(Math.random()*8.0);
		this.setBoy(boy);
		color = Color.LIGHT_GRAY;
		this.risk = risk;
		listOfContacts = new ArrayList<Person>();
		listOfAllContacts = new ArrayList<Person>();
		if (boy){ 
			
		}
		else{
			this.attractiveness = attractiveness;
		}
				
	}
	
	// Get x position
	
	public int getX() {
		return x;
	}
	
	// Get y position
	
	public int getY() {
		return y;
	}

	public Color getColor() {
		return color;
	}
	
	public void setColor(Color c) {
		color = c;
	}
	
	public void step(boolean t) {
	
		// Turn at random
		double random =  Math.random();
		if ( random < 0.33 ) {
			facing = ( facing + 1 ) % 8;
		} else if ( random < 0.66 ) {
			facing = ( facing + 7 ) % 8;
		}
			
		// Take a step			
		if ( facing < 3 ) {
			y = ( y - 1 + world.size() ) % world.size();
		}
		if ( facing > 3 & facing < 7 ) {
			y = ( y + 1 ) % world.size();
		}
		if ( facing > 1 & facing < 5 ) {
			x = ( x + 1 ) % world.size();
		}
		if ( facing == 0 | facing > 5 ) {
			x = ( x - 1 + world.size() ) % world.size();
		}
						
	}

	public int getAttractiveness() {
		
		return attractiveness;
	}

	public Boolean getBoy() {
		return boy;
	}

	public void setBoy(Boolean boy) {
		this.boy = boy;
	}

	public int getID() {
		return id;
	}

	
	
	
}
			