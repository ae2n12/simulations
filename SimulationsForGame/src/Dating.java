import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;


public class Dating {

	public static ArrayList<Man> men;
	public static ArrayList<Woman> women;
	public static int numberOfMen;
	public static int numberOfWomen;
	static JFrame welcomeFrm;
	private static JLabel lblWelcome;
	private static JButton btnNewSimulation;
	private static JButton btn100Simulations;
	static JFrame gameFrame;
	
	public static void main(String[] args) {
		
		initialiseScreen();

		btnNewSimulation.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				gameFrame = new JFrame("Results");
				gameFrame.getContentPane().setBackground(new Color(50, 205, 50));
				gameFrame.setSize(1639, 1470);
				gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				gameFrame.getContentPane().setLayout(new FlowLayout());
				
				
				men = new ArrayList<Man>();
				women = new ArrayList<Woman>();
				numberOfMen = 10;
				numberOfWomen = 10;

				//Create men
				for (int i = 0; i<numberOfMen; i++){
					Man man = new Man(MaleType.values()[i%10]);
					men.add(man);
				}
				
				for (int i = 0; i<numberOfWomen; i++){
					Woman woman = new Woman(FemaleType.values()[i%10]);
					women.add(woman);
				}
			
			
				PrintWriter writer = null;
				try {
					writer = new PrintWriter("DatingDetails.txt");
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//speed dating - each man should meet each woman
				for (int i = 0; i<men.size(); i++){
					for (Woman w : women){
						date(w, men.get(i), writer);
					}
				}
				
				writer.close();
				
				sortMen();
				
				displayResults();
				
				gameFrame.setVisible(true);
				welcomeFrm.setVisible(false);
			
				PrintWriter writer2 = null;
				
				try {
					writer2 = new PrintWriter("Results of one Simulation.txt");
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				writer2.println("Female Results");
				for (Woman w : women){
					writer2.println(w.toString());
				}
				writer2.println("Male Results");
				for (Man m : men){
					writer2.println(m.toString());
				}
				
				writer2.close();
			}

			private void sortMen() {
				Collections.sort(men, new Comparator<Man>(){
					public int compare(Man man1, Man man2){
						if (man1.disease && !man2.disease)
							return -1;
						if (!man1.disease && man2.disease)
							return 1;
						if (man1.disease && man2.disease){
							if (man1.numberOfSexualEncounters > man2.numberOfSexualEncounters){
								return -1;
							}
							else
								return 1;
						}
						if (!man1.disease && !man2.disease){
							if (man1.numberOfSexualEncounters > man2.numberOfSexualEncounters){
								return -1;
							}
							else
								return 1;
						}
						return 0;
					}
				});
			}

			private void displayResults() {
				JPanel panelEncounters = new JPanel();
				JLabel man0 = new JLabel();
				panelEncounters.add(man0);
				gameFrame.add(panelEncounters);
				
				JPanel panelEncounters1 = new JPanel();
				JLabel man1 = new JLabel();
				panelEncounters1.add(man1);
				gameFrame.add(panelEncounters1);
				
				JPanel panelEncounters2 = new JPanel();
				JLabel man2 = new JLabel();
				panelEncounters2.add(man2);
				gameFrame.add(panelEncounters2);
				
				JPanel panelEncounters3 = new JPanel();
				JLabel man3 = new JLabel();
				panelEncounters3.add(man3);
				gameFrame.add(panelEncounters3);
				
				JPanel panelEncounters4 = new JPanel();
				JLabel man4 = new JLabel();
				panelEncounters4.add(man4);
				gameFrame.add(panelEncounters4);
				
				
				JPanel panelEncounters5 = new JPanel();
				JLabel man5 = new JLabel();
				panelEncounters5.add(man5);
				gameFrame.add(panelEncounters5);
				
				JPanel panelEncounters6 = new JPanel();
				JLabel man6 = new JLabel();
				panelEncounters6.add(man6);
				gameFrame.add(panelEncounters6);
				
				JPanel panelEncounters7 = new JPanel();
				JLabel man7 = new JLabel();
				panelEncounters7.add(man7);
				gameFrame.add(panelEncounters7);
				
				JPanel panelEncounters8 = new JPanel();
				JLabel man8 = new JLabel();
				panelEncounters8.add(man8);
				gameFrame.add(panelEncounters8);
				
				JPanel panelEncounters9 = new JPanel();
				JLabel man9 = new JLabel();
				panelEncounters9.add(man9);
				gameFrame.add(panelEncounters9);
				
				man0.setIcon(men.get(0).getImage());
				man0.setText(men.get(0).toString());
				if (men.get(0).disease)
					man0.setForeground(Color.red);
				man1.setIcon(men.get(1).getImage());
				man1.setText(men.get(1).toString());
				if (men.get(1).disease)
					man1.setForeground(Color.red);
				man2.setIcon(men.get(2).getImage());
				man2.setText(men.get(2).toString());
				if (men.get(2).disease)
					man2.setForeground(Color.red);
				man3.setIcon(men.get(3).getImage());
				man3.setText(men.get(3).toString());
				if (men.get(3).disease)
					man3.setForeground(Color.red);
				man4.setIcon(men.get(4).getImage());
				man4.setText(men.get(4).toString());
				if (men.get(4).disease)
					man4.setForeground(Color.red);
				man5.setIcon(men.get(5).getImage());
				man5.setText(men.get(5).toString());
				if (men.get(5).disease)
					man5.setForeground(Color.red);
				man6.setIcon(men.get(6).getImage());
				man6.setText(men.get(6).toString());
				if (men.get(6).disease)
					man6.setForeground(Color.red);
				man7.setIcon(men.get(7).getImage());
				man7.setText(men.get(7).toString());
				if (men.get(7).disease)
					man7.setForeground(Color.red);
				man8.setIcon(men.get(8).getImage());
				man8.setText(men.get(8).toString());
				if (men.get(8).disease)
					man8.setForeground(Color.red);
				man9.setIcon(men.get(9).getImage());
				man9.setText(men.get(9).toString());
				if (men.get(9).disease)
					man9.setForeground(Color.red);
			}
			
		});
		
		btn100Simulations.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
			
				
				men = new ArrayList<Man>();
				women = new ArrayList<Woman>();
				numberOfMen = 10;
				numberOfWomen = 10;

				//Create men
				for (int i = 0; i<numberOfMen; i++){
					Man man = new Man(MaleType.values()[i%10]);
					men.add(man);
				}
				
				for (int i = 0; i<numberOfWomen; i++){
					Woman woman = new Woman(FemaleType.values()[i%10]);
					women.add(woman);
				}
			
			
			for (int j=0; j<100; j++){
				
				//initialise people's disease for the next simulation
				for (Man m : men){
					m.setDisease();
				}
				for (Woman w: women){
					w.setDisease();
				}
				
				PrintWriter writer = null;
				try {
					writer = new PrintWriter("DatingDetails.txt");
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//speed dating - each man should meet each woman
				for (int i = 0; i<men.size(); i++){
					for (Woman w : women){
						date(w, men.get(i), writer);
					}
				}
				
				writer.close();
			}

			PrintWriter writer = null;
			
			try {
				writer = new PrintWriter("100SimulationsResults.txt");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			writer.println("Men");
			for (Man m : men){
				writer.println(m.toStringForMultipleSimulations());
			}
			writer.println("Women");
			for (Woman w : women){
				writer.println(w.toStringForMultipleSimulations());
			}
			
			for (Man m : men){
				HashSet<FemaleType> womenSet = new HashSet<FemaleType>();
				for(FemaleType f: m.heGotTheDiseaseFrom){
					womenSet.add(f);
				}
				
				writer.println("Man " + m.type + " got infected by: ");
				for (FemaleType f: womenSet){
					writer.println(f);
				}
			}
			
			for (Woman w : women){
				HashSet<MaleType> menSet = new HashSet<MaleType>();
				for(MaleType m: w.sheGotTheDiseaseFrom){
					menSet.add(m);
				}
				
				writer.println("Woman " + w.type + " got infected by: ");
				for(MaleType m: menSet){
					writer.println(m);
				}
			}
			
			
			writer.close();
			}
		});
		
		welcomeFrm.setVisible(true);
		
	}

	private static void initialiseScreen() {
		welcomeFrm = new JFrame("Profiles");
		welcomeFrm.getContentPane().setBackground(new Color(0, 128, 0));
		welcomeFrm.setBounds(100, 100, 1139, 401);
		welcomeFrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		welcomeFrm.getContentPane().setLayout(new FlowLayout());

		lblWelcome = new JLabel("Meet the Characters!");
		lblWelcome.setFont(new Font("Arial Black", Font.BOLD, 15));
		lblWelcome.setForeground(Color.RED);
		lblWelcome.setBounds(187, 67, 247, 26);
		welcomeFrm.getContentPane().add(lblWelcome);

		btnNewSimulation = new JButton("New Simulation");
		btn100Simulations = new JButton("Run 100 Simulations");
		btnNewSimulation.setBounds(187, 124, 170, 32);
		welcomeFrm.getContentPane().add(btnNewSimulation);
		welcomeFrm.getContentPane().add(btn100Simulations);
		
		JPanel wPanel = new JPanel();
		wPanel.add(new JLabel("Women:Type 1	"));
		wPanel.add(new JLabel("															Type 2	"));
		wPanel.add(new JLabel("															Type 3	"));
		wPanel.add(new JLabel("															Type 4	"));
		wPanel.add(new JLabel("															Type 5	"));
		wPanel.add(new JLabel("															Type 6	"));
		wPanel.add(new JLabel("															Type 7	"));
		wPanel.add(new JLabel("															Type 8	"));
		wPanel.add(new JLabel("															Type 9	"));
		wPanel.add(new JLabel("															Type 10	"));
		welcomeFrm.add(wPanel);
		
		JPanel womenPanel = new JPanel();
		womenPanel.add(new JLabel(new ImageIcon("woman1.png")));
		womenPanel.add(new JLabel(new ImageIcon("woman2.png")));
		womenPanel.add(new JLabel(new ImageIcon("woman3.png")));
		womenPanel.add(new JLabel(new ImageIcon("woman4.png")));
		womenPanel.add(new JLabel(new ImageIcon("woman5.png")));
		womenPanel.add(new JLabel(new ImageIcon("woman6.png")));
		womenPanel.add(new JLabel(new ImageIcon("woman7.png")));
		womenPanel.add(new JLabel(new ImageIcon("woman8.png")));
		womenPanel.add(new JLabel(new ImageIcon("woman9.png")));
		womenPanel.add(new JLabel(new ImageIcon("woman10.png")));
		welcomeFrm.add(womenPanel);

		JPanel mPanel = new JPanel();
		mPanel.add(new JLabel("Men:Type 1	"));
		mPanel.add(new JLabel("															Type 2	"));
		mPanel.add(new JLabel("															Type 3	"));
		mPanel.add(new JLabel("															Type 4	"));
		mPanel.add(new JLabel("															Type 5	"));
		mPanel.add(new JLabel("															Type 6	"));
		mPanel.add(new JLabel("															Type 7	"));
		mPanel.add(new JLabel("															Type 8	"));
		mPanel.add(new JLabel("															Type 9	"));
		mPanel.add(new JLabel("															Type 10	"));
		welcomeFrm.add(mPanel);
		
		JPanel menPanel = new JPanel();
		menPanel.add(new JLabel(new ImageIcon("man1.png")));
		menPanel.add(new JLabel(new ImageIcon("man2.png")));
		menPanel.add(new JLabel(new ImageIcon("man3.png")));
		menPanel.add(new JLabel(new ImageIcon("man4.png")));
		menPanel.add(new JLabel(new ImageIcon("man5.png")));
		menPanel.add(new JLabel(new ImageIcon("man6.png")));
		menPanel.add(new JLabel(new ImageIcon("man7.png")));
		menPanel.add(new JLabel(new ImageIcon("man8.png")));
		menPanel.add(new JLabel(new ImageIcon("man9.png")));
		menPanel.add(new JLabel(new ImageIcon("man10.png")));
		welcomeFrm.add(menPanel);
	}

	private static void date(Woman w, Man man, PrintWriter writer) {
		
	
		writer.print("Woman " + w.type + " meets man " + man.type);
		if (man.sexWillingness.get(w.type) > (new Random()).nextDouble()){
			w.incrementNumOfSexualEncounters();
			man.incrementNumOfSexualEncounters();
			writer.print(" and they had sex");
			if (man.condomUseIntentions.get(w.type) > (new Random()).nextDouble()){
				w.incrementNumOfSexualEncountersWithCondom();
				man.incrementNumOfSexualEncountersWithCondom();
				writer.print(" with condom.");
				
			}
			else{
				if (w.disease){
					man.disease = true;
					man.heGotTheDiseaseFrom.add(w.type);
					w.sheGaveTheDiseaseTo.add(man.type);
					man.incrementNumOfDiseases();
					writer.print(" and he got her disease.");	
				}
				else if (man.disease){
					w.disease = true;
					w.sheGotTheDiseaseFrom.add(man.type);
					man.heGaveTheDiseaseTo.add(w.type);
					
					w.incrementNumOfDiseases();
					writer.print(" and she got his disease.");
					
				}
			}
		}
		writer.println();
	}

	
}
