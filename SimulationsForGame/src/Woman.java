import java.util.ArrayList;
import java.util.Random;

import javax.swing.ImageIcon;


public class Woman {

	FemaleType type;
	boolean disease;
	int numOfSexualEncounters;
	int numOfSexualEncountersWithCondom;
	ArrayList<MaleType> sheGotTheDiseaseFrom;
	ArrayList<MaleType> sheGaveTheDiseaseTo;
	ImageIcon image;
	int numOfDiseases;

	public Woman(FemaleType type) {
		
		this.type = type;
		
		setDisease();
		
		sheGotTheDiseaseFrom = new ArrayList<MaleType>();
		sheGaveTheDiseaseTo = new ArrayList<MaleType>();
		
		numOfSexualEncounters = 0;
		numOfSexualEncountersWithCondom =0; 
		
		switch (type){
			case Type1 : {image = new ImageIcon("woman1.png"); break;} 
			case Type2 : {image = new ImageIcon("woman2.png"); break;}
			case Type3 : {image = new ImageIcon("woman3.png"); break;}
			case Type4 : {image = new ImageIcon("woman4.png"); break;}
			case Type5 : {image = new ImageIcon("woman5.png"); break;}
			case Type6 : {image = new ImageIcon("woman6.png"); break;}
			case Type7 : {image = new ImageIcon("woman7.png"); break;}
			case Type8 : {image = new ImageIcon("woman8.png"); break;}
			case Type9 : {image = new ImageIcon("woman9.png"); break;}
			case Type10 : {image = new ImageIcon("woman10.png"); break;}
		}
	}

	public void setDisease() {
		Random rand = new Random();
		double d = rand.nextDouble();
		
		if (d > 0.8){
			disease = true;
		}
		else{
			disease = false;
		}
		
	}

	public void incrementNumOfSexualEncounters() {
		numOfSexualEncounters++;
		
	}

	public void incrementNumOfSexualEncountersWithCondom() {
		numOfSexualEncountersWithCondom++;
		
	}
	
	public String toString(){
		String s = "The woman with " + type + " had " + numOfSexualEncounters + " sexual encounters and " + numOfSexualEncountersWithCondom + " of them were with condom";
		if (disease)
			s += " and she got a disease from " + sheGotTheDiseaseFrom;
		
		return s;
	}
	
	public ImageIcon getImage() {
		return image;
	}

	public void incrementNumOfDiseases() {
		numOfDiseases++;
	}
	
	public String toStringForMultipleSimulations(){
		
		String s = type + " had sex with " + numOfSexualEncounters + " men and used condom with " + numOfSexualEncountersWithCondom + " and she got infected by " + sheGotTheDiseaseFrom.size() + " and she gave the infection to " + sheGaveTheDiseaseTo.size();		                                  				      
		
		return s;
	}
}
