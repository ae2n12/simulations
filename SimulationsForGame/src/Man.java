import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import javax.swing.ImageIcon;


public class Man {

	MaleType type;
	HashMap<FemaleType, Double> sexWillingness;
	HashMap<FemaleType, Double> condomUseIntentions;
	boolean disease;
	int numberOfSexualEncounters;
	int numOfSexualEncountersWithCondom;
	ArrayList<FemaleType> heGotTheDiseaseFrom;
	ArrayList<FemaleType> heGaveTheDiseaseTo;
	ImageIcon image;
	int numOfDiseases;
	
	public Man(MaleType type,
			HashMap<FemaleType, Double> sexWillingness,
			HashMap<FemaleType, Double> condomUseIntentions) {
		this.type = type;
		this.sexWillingness = sexWillingness;
		this.condomUseIntentions = condomUseIntentions;
	
		setDisease();
		
		heGotTheDiseaseFrom = new ArrayList<FemaleType>();
		heGaveTheDiseaseTo = new ArrayList<FemaleType>();
		
		numberOfSexualEncounters = 0 ;
		numOfSexualEncountersWithCondom = 0;
		
		switch (type){
		case Type1 : image = new ImageIcon("man1.png"); 
		case Type2 : image = new ImageIcon("man2.png"); 
		case Type3 : image = new ImageIcon("man3.png"); 
		case Type4 : image = new ImageIcon("man4.png"); 
		case Type5 : image = new ImageIcon("man5.png"); 
		case Type6 : image = new ImageIcon("man6.png"); 
		case Type7 : image = new ImageIcon("man7.png"); 
		case Type8 : image = new ImageIcon("man8.png"); 
		case Type9 : image = new ImageIcon("man9.png"); 
		case Type10 : image = new ImageIcon("man10.png"); 
	}
	}

	public void setDisease() {
		Random rand = new Random();
		
		if (rand.nextDouble() > 0.8){
			disease = true;
		}
		else{
			disease = false;
		}
	}

	public Man(MaleType type) {
		
		this.type = type;
		sexWillingness = new HashMap<FemaleType, Double>();
		condomUseIntentions = new HashMap<FemaleType, Double>();
		setDisease();
		
		heGotTheDiseaseFrom = new ArrayList<FemaleType>();
		heGaveTheDiseaseTo = new ArrayList<FemaleType>();
		
		
		switch(type){
			case Type1: {
				image = new ImageIcon("man1.png"); 
				initialiseSexWillingnessForType1();
				initialiseCondomUseIntentionsForType1();
				break;
			}
			case Type2: {
				image = new ImageIcon("man2.png"); 
				initialiseSexWillingnessForType2();
				initialiseCondomUseIntentionsForType2();
				break;
			}
			case Type3: {
				image = new ImageIcon("man3.png"); 
				initialiseSexWillingnessForType3();
				initialiseCondomUseIntentionsForType3();
				break;
			}
			case Type4: {
				image = new ImageIcon("man4.png"); 
				initialiseSexWillingnessForType4();
				initialiseCondomUseIntentionsForType4();
				break;
			}
			case Type5: {
				image = new ImageIcon("man5.png"); 
				initialiseSexWillingnessForType5();
				initialiseCondomUseIntentionsForType5();
				break;
			}
			case Type6: {
				image = new ImageIcon("man6.png"); 
				initialiseSexWillingnessForType6();
				initialiseCondomUseIntentionsForType6();
				break;
			}
			case Type7: {
				image = new ImageIcon("man7.png"); 
				initialiseSexWillingnessForType7();
				initialiseCondomUseIntentionsForType7();
				break;
			}
			case Type8: {
				image = new ImageIcon("man8.png"); 
				initialiseSexWillingnessForType8();
				initialiseCondomUseIntentionsForType8();
				break;
			}
			case Type9: {
				image = new ImageIcon("man9.png"); 
				initialiseSexWillingnessForType9();
				initialiseCondomUseIntentionsForType9();
				break;
			}
			case Type10: {
				image = new ImageIcon("man10.png"); 
				initialiseSexWillingnessForType10();
				initialiseCondomUseIntentionsForType10();
				break;
			}
		}
		
	}

	//TODO : Put the actual values
	
	private void initialiseSexWillingnessForType1() {
		
		sexWillingness.put(FemaleType.values()[0], 0.01);
		sexWillingness.put(FemaleType.values()[1], 0.0);
		sexWillingness.put(FemaleType.values()[2], 0.03);
		sexWillingness.put(FemaleType.values()[3], 0.03);
		sexWillingness.put(FemaleType.values()[4], 0.03);
		sexWillingness.put(FemaleType.values()[5], 0.02);
		sexWillingness.put(FemaleType.values()[6], 0.03);
		sexWillingness.put(FemaleType.values()[7], 0.03);
		sexWillingness.put(FemaleType.values()[8], 0.03);
		sexWillingness.put(FemaleType.values()[9], 0.0);
		
	}
	
	private void initialiseCondomUseIntentionsForType1() {
		for (int i=0; i< FemaleType.values().length; i++){
			condomUseIntentions.put(FemaleType.values()[i], 1.0);
		}
	}

	
	private void initialiseSexWillingnessForType2() {
		sexWillingness.put(FemaleType.values()[0], 0.84);
		sexWillingness.put(FemaleType.values()[1], 0.93);
		sexWillingness.put(FemaleType.values()[2], 0.83);
		sexWillingness.put(FemaleType.values()[3], 0.94);
		sexWillingness.put(FemaleType.values()[4], 1.0);
		sexWillingness.put(FemaleType.values()[5], 0.97);
		sexWillingness.put(FemaleType.values()[6], 0.99);
		sexWillingness.put(FemaleType.values()[7], 0.96);
		sexWillingness.put(FemaleType.values()[8], 0.95);
		sexWillingness.put(FemaleType.values()[9], 0.96);
		
	}
	
	private void initialiseCondomUseIntentionsForType2() {
		
		condomUseIntentions.put(FemaleType.values()[0], 0.08);
		condomUseIntentions.put(FemaleType.values()[1], 0.04);
		condomUseIntentions.put(FemaleType.values()[2], 0.07);
		condomUseIntentions.put(FemaleType.values()[3], 0.96);
		condomUseIntentions.put(FemaleType.values()[4], 0.97);
		condomUseIntentions.put(FemaleType.values()[5], 0.06);
		condomUseIntentions.put(FemaleType.values()[6], 0.01);
		condomUseIntentions.put(FemaleType.values()[7], 0.04);
		condomUseIntentions.put(FemaleType.values()[8], 0.07);
		condomUseIntentions.put(FemaleType.values()[9], 0.05);

	}
	
	
	private void initialiseSexWillingnessForType3() {
		sexWillingness.put(FemaleType.values()[0], 0.05);
		sexWillingness.put(FemaleType.values()[1], 0.06);
		sexWillingness.put(FemaleType.values()[2], 0.21);
		sexWillingness.put(FemaleType.values()[3], 0.30);
		sexWillingness.put(FemaleType.values()[4], 0.18);
		sexWillingness.put(FemaleType.values()[5], 0.15);
		sexWillingness.put(FemaleType.values()[6], 0.09);
		sexWillingness.put(FemaleType.values()[7], 0.18);
		sexWillingness.put(FemaleType.values()[8], 0.08);
		sexWillingness.put(FemaleType.values()[9], 0.11);
		
	}
	
	private void initialiseCondomUseIntentionsForType3() {
		for (int i=0; i< FemaleType.values().length; i++){
			condomUseIntentions.put(FemaleType.values()[i], 1.0);
		}
	}
	
	
	private void initialiseSexWillingnessForType4() {
		sexWillingness.put(FemaleType.values()[0], 0.80);
		sexWillingness.put(FemaleType.values()[1], 0.50);
		sexWillingness.put(FemaleType.values()[2], 0.75);
		sexWillingness.put(FemaleType.values()[3], 0.97);
		sexWillingness.put(FemaleType.values()[4], 0.88);
		sexWillingness.put(FemaleType.values()[5], 0.84);
		sexWillingness.put(FemaleType.values()[6], 0.88);
		sexWillingness.put(FemaleType.values()[7], 0.85);
		sexWillingness.put(FemaleType.values()[8], 0.77);
		sexWillingness.put(FemaleType.values()[9], 0.86);
		
	}
	
	private void initialiseCondomUseIntentionsForType4() {
		for (int i=0; i< FemaleType.values().length; i++){
			condomUseIntentions.put(FemaleType.values()[i], 1.0);
		}
	}
	
	
	private void initialiseSexWillingnessForType5() {
		sexWillingness.put(FemaleType.values()[0], 0.04);
		sexWillingness.put(FemaleType.values()[1], 0.12);
		sexWillingness.put(FemaleType.values()[2], 0.28);
		sexWillingness.put(FemaleType.values()[3], 0.20);
		sexWillingness.put(FemaleType.values()[4], 0.20);
		sexWillingness.put(FemaleType.values()[5], 0.19);
		sexWillingness.put(FemaleType.values()[6], 0.20);
		sexWillingness.put(FemaleType.values()[7], 0.16);
		sexWillingness.put(FemaleType.values()[8], 0.16);
		sexWillingness.put(FemaleType.values()[9], 0.16);			
	}
	
	private void initialiseCondomUseIntentionsForType5() {
		for (int i=0; i< FemaleType.values().length; i++){
			condomUseIntentions.put(FemaleType.values()[i], 1.0);
		}
	}
	
	private void initialiseSexWillingnessForType6() {
		sexWillingness.put(FemaleType.values()[0], 0.60);
		sexWillingness.put(FemaleType.values()[1], 0.18);
		sexWillingness.put(FemaleType.values()[2], 0.24);
		sexWillingness.put(FemaleType.values()[3], 0.92);
		sexWillingness.put(FemaleType.values()[4], 0.73);
		sexWillingness.put(FemaleType.values()[5], 0.23);
		sexWillingness.put(FemaleType.values()[6], 0.72);
		sexWillingness.put(FemaleType.values()[7], 0.43);
		sexWillingness.put(FemaleType.values()[8], 0.39);
		sexWillingness.put(FemaleType.values()[9], 0.33);			
	}
	
	private void initialiseCondomUseIntentionsForType6() {
		condomUseIntentions.put(FemaleType.values()[0], 1.0);
		condomUseIntentions.put(FemaleType.values()[1], 1.0);
		condomUseIntentions.put(FemaleType.values()[2], 1.0);
		condomUseIntentions.put(FemaleType.values()[3], 0.80);
		condomUseIntentions.put(FemaleType.values()[4], 0.91);
		condomUseIntentions.put(FemaleType.values()[5], 1.00);
		condomUseIntentions.put(FemaleType.values()[6], 0.90);
		condomUseIntentions.put(FemaleType.values()[7], 1.0);
		condomUseIntentions.put(FemaleType.values()[8], 1.0);
		condomUseIntentions.put(FemaleType.values()[9], 1.0);
	}
	
	
	private void initialiseSexWillingnessForType7() {
		
			sexWillingness.put(FemaleType.values()[0], 0.05);
			sexWillingness.put(FemaleType.values()[1], 0.01);
			sexWillingness.put(FemaleType.values()[2], 0.32);
			sexWillingness.put(FemaleType.values()[3], 0.13);
			sexWillingness.put(FemaleType.values()[4], 0.50);
			sexWillingness.put(FemaleType.values()[5], 0.26);
			sexWillingness.put(FemaleType.values()[6], 0.39);
			sexWillingness.put(FemaleType.values()[7], 0.11);
			sexWillingness.put(FemaleType.values()[8], 0.17);
			sexWillingness.put(FemaleType.values()[9], 0.06);						
		
	}
	
	private void initialiseCondomUseIntentionsForType7() {
		for (int i=0; i< FemaleType.values().length; i++){
			condomUseIntentions.put(FemaleType.values()[i], 0.99);
		}
	}

	private void initialiseSexWillingnessForType8() {
		sexWillingness.put(FemaleType.values()[0], 0.0);
		sexWillingness.put(FemaleType.values()[1], 0.36);
		sexWillingness.put(FemaleType.values()[2], 0.79);
		sexWillingness.put(FemaleType.values()[3], 0.99);
		sexWillingness.put(FemaleType.values()[4], 0.85);
		sexWillingness.put(FemaleType.values()[5], 0.83);
		sexWillingness.put(FemaleType.values()[6], 0.20);
		sexWillingness.put(FemaleType.values()[7], 0.30);
		sexWillingness.put(FemaleType.values()[8], 0.29);
		sexWillingness.put(FemaleType.values()[9], 0.34);			
		
	}
	
	private void initialiseCondomUseIntentionsForType8() {
		condomUseIntentions.put(FemaleType.values()[0], 0.98);
		condomUseIntentions.put(FemaleType.values()[1], 0.98);
		condomUseIntentions.put(FemaleType.values()[2], 0.88);
		condomUseIntentions.put(FemaleType.values()[3], 0.15);
		condomUseIntentions.put(FemaleType.values()[4], 0.28);
		condomUseIntentions.put(FemaleType.values()[5], 0.56);
		condomUseIntentions.put(FemaleType.values()[6], 0.42);
		condomUseIntentions.put(FemaleType.values()[7], 0.99);
		condomUseIntentions.put(FemaleType.values()[8], 0.98);
		condomUseIntentions.put(FemaleType.values()[9], 1.0);
	}
	
	
	private void initialiseSexWillingnessForType9() {
		sexWillingness.put(FemaleType.values()[0], 0.0);
		sexWillingness.put(FemaleType.values()[1], 0.0);
		sexWillingness.put(FemaleType.values()[2], 0.21);
		sexWillingness.put(FemaleType.values()[3], 0.03);
		sexWillingness.put(FemaleType.values()[4], 0.84);
		sexWillingness.put(FemaleType.values()[5], 0.08);
		sexWillingness.put(FemaleType.values()[6], 0.36);
		sexWillingness.put(FemaleType.values()[7], 0.61);
		sexWillingness.put(FemaleType.values()[8], 0.06);
		sexWillingness.put(FemaleType.values()[9], 0.19);			
	}
	
	private void initialiseCondomUseIntentionsForType9() {
		condomUseIntentions.put(FemaleType.values()[0], 1.0);
		condomUseIntentions.put(FemaleType.values()[1], 1.0);
		condomUseIntentions.put(FemaleType.values()[2], 1.0);
		condomUseIntentions.put(FemaleType.values()[3], 0.98);
		condomUseIntentions.put(FemaleType.values()[4], 0.92);
		condomUseIntentions.put(FemaleType.values()[5], 1.00);
		condomUseIntentions.put(FemaleType.values()[6], 0.94);
		condomUseIntentions.put(FemaleType.values()[7], 0.97);
		condomUseIntentions.put(FemaleType.values()[8], 1.0);
		condomUseIntentions.put(FemaleType.values()[9], 1.0);
	}
	
	private void initialiseSexWillingnessForType10() {
		sexWillingness.put(FemaleType.values()[0], 0.67);
		sexWillingness.put(FemaleType.values()[1], 0.05);
		sexWillingness.put(FemaleType.values()[2], 0.31);
		sexWillingness.put(FemaleType.values()[3], 1.0);
		sexWillingness.put(FemaleType.values()[4], 0.68);
		sexWillingness.put(FemaleType.values()[5], 1.0);
		sexWillingness.put(FemaleType.values()[6], 0.7);
		sexWillingness.put(FemaleType.values()[7], 0.12);
		sexWillingness.put(FemaleType.values()[8], 0.24);
		sexWillingness.put(FemaleType.values()[9], 0.13);			
		
	}
	
	private void initialiseCondomUseIntentionsForType10() {
		condomUseIntentions.put(FemaleType.values()[0], 0.96);
		condomUseIntentions.put(FemaleType.values()[1], 1.0);
		condomUseIntentions.put(FemaleType.values()[2], 1.0);
		condomUseIntentions.put(FemaleType.values()[3], 0.96);
		condomUseIntentions.put(FemaleType.values()[4], 1.0);
		condomUseIntentions.put(FemaleType.values()[5], 0.95);
		condomUseIntentions.put(FemaleType.values()[6], 0.95);
		condomUseIntentions.put(FemaleType.values()[7], 1.0);
		condomUseIntentions.put(FemaleType.values()[8], 1.0);
		condomUseIntentions.put(FemaleType.values()[9], 0.96);
	}

	public void incrementNumOfSexualEncounters() {
		numberOfSexualEncounters++;
	}

	public void incrementNumOfSexualEncountersWithCondom() {
		numOfSexualEncountersWithCondom++;
		
	}


	public String toString(){
		
		String s = type + " had sex with " + numberOfSexualEncounters + " women and used condom with " + numOfSexualEncountersWithCondom;
		if (disease)
			s += " and he got a disease from " + heGotTheDiseaseFrom;
		else
			s += " 			                                  				      ";
		
		return s;
	}


	public ImageIcon getImage() {
		return image;
	}

	public void incrementNumOfDiseases() {
		numOfDiseases++;
	}

	public String toStringForMultipleSimulations(){
		
		String s = type + " had sex with " + numberOfSexualEncounters + " women and used condom with " + numOfSexualEncountersWithCondom + " and he got infected by " + heGotTheDiseaseFrom.size() + " and he gave the infection to " + heGaveTheDiseaseTo.size();		                                  				      
		
		return s;
	}
	
	
}
