
public class Encounter {
	
	Person a;
	Person b;
	boolean infectionHappened;
	boolean condomHasBeenUsed;
	Person infected;
	
	public Encounter(Person a, Person b){
		this.a = a;
		this.b = b;
		
		a.numOfPeopleMet++;
		b.numOfPeopleMet++;
		
		if (a.chanceOfCondomUse < 75 && b.chanceOfCondomUse < 75){
			if (a.infected && b.infected){
				//Do nothing
			}
			else if (a.infected){
				b.infected = true;
				infected = b;
				infectionHappened = true;
			}
			else if (b.infected){
				a.infected = true;
				infected = a;
				infectionHappened = true;
			}
		}
		else{
			condomHasBeenUsed = true;
			a.numOfEncountersWithCondom++;
			b.numOfEncountersWithCondom++;
		}
	}
	
	public String toString(){
		return a.name + " + " + b.name + (condomHasBeenUsed? " condom " : " -----" ) + (infectionHappened? ("----> " +infected.name + " was infected") : "");
	}

}
