
public class Person {

	char name;
	boolean infected;
	double chanceOfCondomUse;
	int numOfPeopleMet;
	double numOfEncountersWithCondom;
	
	public Person(char name, double c){
		this.name = name;
		chanceOfCondomUse = c;
		System.out.println(c);
	}
	
	public String getNameAndStatus(){
		return name + "(" + (infected? "infected" : "not infected") + ")";
	}
	
	public String toString(){
		return name + " infected: " + infected + ", condom use percentage: " + (numOfEncountersWithCondom/numOfPeopleMet);
	}

}
