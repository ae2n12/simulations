
public enum FemaleType {

	Type1, Type2, Type3, Type4, Type5, Type6, Type7, Type8, Type9, Type10;

	public int compare(FemaleType a, FemaleType b){
		if (a.ordinal()<b.ordinal())
			return -1;
		if (b.ordinal()<a.ordinal()){
			return 1;
		}
		else
			return 0;
	}
}
