package RescaleValues;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Rescaler {

	/**
	 * @param args
	 */
	
	static double amount;
	static double[][] units;
	static int[] numOfUnits;
	static double[] unitValue;
	
	public static void main(String[] args) {
		
		units = new double[10][9];
		numOfUnits = new int[9];
		unitValue = new double[9];
		amount = 0.45; //update the amount accordingly - average condom use is 0.84, average sex willingness is 0.45
		
		Scanner scan = null;
		try {
			scan = new Scanner(new File("inputSexWillingness.txt"));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//Initialise it with 0
		for (int i=0; i<10; i++){
			for (int j=0; j<9; j++){
				units[i][j] = Double.parseDouble(scan.next());
				numOfUnits[j] = 0;
			}
		}
		
		
		//Count the units
		for (int i=0; i<9; i++){
			for(int j=0; j<10; j++){
				numOfUnits[i] += units[j][i];
			}
		}
		
		//update the units
		for (int i=0; i<9; i++){
			unitValue[i] = amount/numOfUnits[i];
		}
		
		for (int i=0; i<10; i++){
			for (int j=0; j<9; j++){
				units[i][j] *= unitValue[j];
			}
		}
		
		try {
			PrintWriter writer = new PrintWriter("result.txt");
			for (int i=0; i<10; i++){
				for (int j=0; j<9; j++){
					 writer.printf("%3f	",(units[i][j]*10));
				}
				writer.println();
			}
			writer.close();
			
			
			PrintWriter writer2 = new PrintWriter("wordyResult.txt");
			for (int i=0; i<9; i++){
				writer2.println("Man Type: " + (i+1));
				for (int j=0; j<10; j++){
					 writer2.printf("sexWillingness.put(FemaleType.values()[" + j + "], %3f);\n", (units[j][i]*10));
				}
				writer2.println();
			}
			writer2.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
