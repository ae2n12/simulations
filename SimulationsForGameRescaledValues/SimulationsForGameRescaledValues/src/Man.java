import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.swing.ImageIcon;


public class Man {

	MaleType type;
	HashMap<FemaleType, Double> sexWillingness;
	HashMap<FemaleType, Double> condomUseIntentions;
	boolean disease;
	int numberOfSexualEncounters;
	int numOfSexualEncountersWithCondom;
	SortedSet<FemaleType> heGotTheDiseaseFrom;
	SortedSet<FemaleType> heGaveTheDiseaseTo;
	ImageIcon image;
	int numOfDiseases;
	int clones;
	protected boolean printed;
	protected boolean printedDisease;
	
	public Man(MaleType type,
			HashMap<FemaleType, Double> sexWillingness,
			HashMap<FemaleType, Double> condomUseIntentions) {
		this.type = type;
		this.sexWillingness = sexWillingness;
		this.condomUseIntentions = condomUseIntentions;
	
		setDisease();
		
		heGotTheDiseaseFrom = new TreeSet<FemaleType>();
		heGaveTheDiseaseTo = new TreeSet<FemaleType>();
		
		numberOfSexualEncounters = 0 ;
		numOfSexualEncountersWithCondom = 0;
		
		switch (type){
		case TypeA : image = new ImageIcon("man1.png"); 
		case TypeB : image = new ImageIcon("man2.png"); 
		case TypeC : image = new ImageIcon("man3.png"); 
		case TypeD : image = new ImageIcon("man4.png"); 
		case TypeE : image = new ImageIcon("man5.png"); 
		case TypeF : image = new ImageIcon("man6.png"); 
		case TypeG : image = new ImageIcon("man7.png"); 
		case TypeH : image = new ImageIcon("man8.png"); 
		case TypeI : image = new ImageIcon("man9.png"); 
	}
	}

	public void setDisease() {
		Random rand = new Random();
		
		if (rand.nextDouble() > 0.8){
			disease = true;
		}
		else{
			disease = false;
		}
	}

	public Man(MaleType type) {
		
		this.type = type;
		sexWillingness = new HashMap<FemaleType, Double>();
		condomUseIntentions = new HashMap<FemaleType, Double>();
		setDisease();
		
		heGotTheDiseaseFrom = new TreeSet<FemaleType>();
		heGaveTheDiseaseTo = new TreeSet<FemaleType>();
		
		
		switch(type){
			case TypeA: {
				image = new ImageIcon("man1.png"); 
				initialiseSexWillingnessForType1();
				initialiseCondomUseIntentionsForType1();
				break;
			}
			case TypeB: {
				image = new ImageIcon("man2.png"); 
				initialiseSexWillingnessForType2();
				initialiseCondomUseIntentionsForType2();
				break;
			}
			case TypeC: {
				image = new ImageIcon("man3.png"); 
				initialiseSexWillingnessForType3();
				initialiseCondomUseIntentionsForType3();
				break;
			}
			case TypeD: {
				image = new ImageIcon("man4.png"); 
				initialiseSexWillingnessForType4();
				initialiseCondomUseIntentionsForType4();
				break;
			}
			case TypeE: {
				image = new ImageIcon("man5.png"); 
				initialiseSexWillingnessForType5();
				initialiseCondomUseIntentionsForType5();
				break;
			}
			case TypeF: {
				image = new ImageIcon("man6.png"); 
				initialiseSexWillingnessForType6();
				initialiseCondomUseIntentionsForType6();
				break;
			}
			case TypeG: {
				image = new ImageIcon("man7.png"); 
				initialiseSexWillingnessForType7();
				initialiseCondomUseIntentionsForType7();
				break;
			}
			case TypeH: {
				image = new ImageIcon("man8.png"); 
				initialiseSexWillingnessForType8();
				initialiseCondomUseIntentionsForType8();
				break;
			}
			case TypeI: {
				image = new ImageIcon("man9.png"); 
				initialiseSexWillingnessForType9();
				initialiseCondomUseIntentionsForType9();
				break;
			}
			
		}
		
	}

	
	private void initialiseSexWillingnessForType1() {
		
		sexWillingness.put(FemaleType.values()[0], 0.195652);
		sexWillingness.put(FemaleType.values()[1], 0.391304);
		sexWillingness.put(FemaleType.values()[2], 0.195652);
		sexWillingness.put(FemaleType.values()[3], 0.391304);
		sexWillingness.put(FemaleType.values()[4], 0.586957);
		sexWillingness.put(FemaleType.values()[5], 0.586957);
		sexWillingness.put(FemaleType.values()[6], 0.586957);
		sexWillingness.put(FemaleType.values()[7], 0.586957);
		sexWillingness.put(FemaleType.values()[8], 0.391304);
		sexWillingness.put(FemaleType.values()[9], 0.586957);

		
	}
	
	private void initialiseCondomUseIntentionsForType1() {
		condomUseIntentions.put(FemaleType.values()[0], 0.884211);
		condomUseIntentions.put(FemaleType.values()[1], 0.442105);
		condomUseIntentions.put(FemaleType.values()[2], 0.884211);
		condomUseIntentions.put(FemaleType.values()[3], 1.326316);
		condomUseIntentions.put(FemaleType.values()[4], 1.326316);
		condomUseIntentions.put(FemaleType.values()[5], 0.884211);
		condomUseIntentions.put(FemaleType.values()[6], 0.442105);
		condomUseIntentions.put(FemaleType.values()[7], 0.442105);
		condomUseIntentions.put(FemaleType.values()[8], 0.884211);
		condomUseIntentions.put(FemaleType.values()[9], 0.884211);
	}

	
	private void initialiseSexWillingnessForType2() {
		sexWillingness.put(FemaleType.values()[0], 0.264706);
		sexWillingness.put(FemaleType.values()[1], 0.264706);
		sexWillingness.put(FemaleType.values()[2], 0.794118);
		sexWillingness.put(FemaleType.values()[3], 0.794118);
		sexWillingness.put(FemaleType.values()[4], 0.529412);
		sexWillingness.put(FemaleType.values()[5], 0.529412);
		sexWillingness.put(FemaleType.values()[6], 0.264706);
		sexWillingness.put(FemaleType.values()[7], 0.529412);
		sexWillingness.put(FemaleType.values()[8], 0.264706);
		sexWillingness.put(FemaleType.values()[9], 0.264706);

	}
	
	private void initialiseCondomUseIntentionsForType2() {
		
		condomUseIntentions.put(FemaleType.values()[0], 0.840000);
		condomUseIntentions.put(FemaleType.values()[1], 0.840000);
		condomUseIntentions.put(FemaleType.values()[2], 0.840000);
		condomUseIntentions.put(FemaleType.values()[3], 0.840000);
		condomUseIntentions.put(FemaleType.values()[4], 0.840000);
		condomUseIntentions.put(FemaleType.values()[5], 0.840000);
		condomUseIntentions.put(FemaleType.values()[6], 0.840000);
		condomUseIntentions.put(FemaleType.values()[7], 0.840000);
		condomUseIntentions.put(FemaleType.values()[8], 0.840000);
		condomUseIntentions.put(FemaleType.values()[9], 0.840000);

	}
	
	
	private void initialiseSexWillingnessForType3() {
		sexWillingness.put(FemaleType.values()[0], 0.236842);
		sexWillingness.put(FemaleType.values()[1], 0.236842);
		sexWillingness.put(FemaleType.values()[2], 0.710526);
		sexWillingness.put(FemaleType.values()[3], 0.473684);
		sexWillingness.put(FemaleType.values()[4], 0.473684);
		sexWillingness.put(FemaleType.values()[5], 0.473684);
		sexWillingness.put(FemaleType.values()[6], 0.473684);
		sexWillingness.put(FemaleType.values()[7], 0.473684);
		sexWillingness.put(FemaleType.values()[8], 0.473684);
		sexWillingness.put(FemaleType.values()[9], 0.473684);

	}
	
	private void initialiseCondomUseIntentionsForType3() {
		condomUseIntentions.put(FemaleType.values()[0], 0.840000);
		condomUseIntentions.put(FemaleType.values()[1], 0.840000);
		condomUseIntentions.put(FemaleType.values()[2], 0.840000);
		condomUseIntentions.put(FemaleType.values()[3], 0.840000);
		condomUseIntentions.put(FemaleType.values()[4], 0.840000);
		condomUseIntentions.put(FemaleType.values()[5], 0.840000);
		condomUseIntentions.put(FemaleType.values()[6], 0.840000);
		condomUseIntentions.put(FemaleType.values()[7], 0.840000);
		condomUseIntentions.put(FemaleType.values()[8], 0.840000);
		condomUseIntentions.put(FemaleType.values()[9], 0.840000);

	}
	
	
	private void initialiseSexWillingnessForType4() {
		sexWillingness.put(FemaleType.values()[0], 0.236842);
		sexWillingness.put(FemaleType.values()[1], 0.236842);
		sexWillingness.put(FemaleType.values()[2], 0.710526);
		sexWillingness.put(FemaleType.values()[3], 0.473684);
		sexWillingness.put(FemaleType.values()[4], 0.710526);
		sexWillingness.put(FemaleType.values()[5], 0.473684);
		sexWillingness.put(FemaleType.values()[6], 0.710526);
		sexWillingness.put(FemaleType.values()[7], 0.236842);
		sexWillingness.put(FemaleType.values()[8], 0.473684);
		sexWillingness.put(FemaleType.values()[9], 0.236842);		
	}
	
	private void initialiseCondomUseIntentionsForType4() {
		condomUseIntentions.put(FemaleType.values()[0], 0.840000);
		condomUseIntentions.put(FemaleType.values()[1], 0.840000);
		condomUseIntentions.put(FemaleType.values()[2], 0.840000);
		condomUseIntentions.put(FemaleType.values()[3], 0.840000);
		condomUseIntentions.put(FemaleType.values()[4], 0.840000);
		condomUseIntentions.put(FemaleType.values()[5], 0.840000);
		condomUseIntentions.put(FemaleType.values()[6], 0.840000);
		condomUseIntentions.put(FemaleType.values()[7], 0.840000);
		condomUseIntentions.put(FemaleType.values()[8], 0.840000);
		condomUseIntentions.put(FemaleType.values()[9], 0.840000);
	}
	
	
	private void initialiseSexWillingnessForType5() {
		sexWillingness.put(FemaleType.values()[0], 0.195652);
		sexWillingness.put(FemaleType.values()[1], 0.391304);
		sexWillingness.put(FemaleType.values()[2], 0.586957);
		sexWillingness.put(FemaleType.values()[3], 0.586957);
		sexWillingness.put(FemaleType.values()[4], 0.586957);
		sexWillingness.put(FemaleType.values()[5], 0.586957);
		sexWillingness.put(FemaleType.values()[6], 0.391304);
		sexWillingness.put(FemaleType.values()[7], 0.391304);
		sexWillingness.put(FemaleType.values()[8], 0.391304);
		sexWillingness.put(FemaleType.values()[9], 0.391304);
	}
	
	private void initialiseCondomUseIntentionsForType5() {
		condomUseIntentions.put(FemaleType.values()[0], 1.050000);
		condomUseIntentions.put(FemaleType.values()[1], 1.050000);
		condomUseIntentions.put(FemaleType.values()[2], 1.050000);
		condomUseIntentions.put(FemaleType.values()[3], 0.350000);
		condomUseIntentions.put(FemaleType.values()[4], 0.350000);
		condomUseIntentions.put(FemaleType.values()[5], 0.700000);
		condomUseIntentions.put(FemaleType.values()[6], 0.700000);
		condomUseIntentions.put(FemaleType.values()[7], 1.050000);
		condomUseIntentions.put(FemaleType.values()[8], 1.050000);
		condomUseIntentions.put(FemaleType.values()[9], 1.050000);
	}
	
	private void initialiseSexWillingnessForType6() {
		sexWillingness.put(FemaleType.values()[0], 0.250000);
		sexWillingness.put(FemaleType.values()[1], 0.250000);
		sexWillingness.put(FemaleType.values()[2], 0.500000);
		sexWillingness.put(FemaleType.values()[3], 0.250000);
		sexWillingness.put(FemaleType.values()[4], 0.750000);
		sexWillingness.put(FemaleType.values()[5], 0.250000);
		sexWillingness.put(FemaleType.values()[6], 0.750000);
		sexWillingness.put(FemaleType.values()[7], 0.750000);
		sexWillingness.put(FemaleType.values()[8], 0.250000);
		sexWillingness.put(FemaleType.values()[9], 0.500000);


	}
	
	private void initialiseCondomUseIntentionsForType6() {
		condomUseIntentions.put(FemaleType.values()[0], 1.050000);
		condomUseIntentions.put(FemaleType.values()[1], 1.050000);
		condomUseIntentions.put(FemaleType.values()[2], 1.050000);
		condomUseIntentions.put(FemaleType.values()[3], 0.700000);
		condomUseIntentions.put(FemaleType.values()[4], 0.350000);
		condomUseIntentions.put(FemaleType.values()[5], 1.050000);
		condomUseIntentions.put(FemaleType.values()[6], 0.350000);
		condomUseIntentions.put(FemaleType.values()[7], 0.700000);
		condomUseIntentions.put(FemaleType.values()[8], 1.050000);
		condomUseIntentions.put(FemaleType.values()[9], 1.050000);

	}
	
	
	private void initialiseSexWillingnessForType7() {
		
		sexWillingness.put(FemaleType.values()[0], 0.0);
		sexWillingness.put(FemaleType.values()[1], 0.0);
		sexWillingness.put(FemaleType.values()[2], 0.0);
		sexWillingness.put(FemaleType.values()[3], 0.0);
		sexWillingness.put(FemaleType.values()[4], 0.0);
		sexWillingness.put(FemaleType.values()[5], 0.0);
		sexWillingness.put(FemaleType.values()[6], 0.0);
		sexWillingness.put(FemaleType.values()[7], 0.0);
		sexWillingness.put(FemaleType.values()[8], 0.0);
		sexWillingness.put(FemaleType.values()[9], 0.0);
	}
	
	private void initialiseCondomUseIntentionsForType7() {
		condomUseIntentions.put(FemaleType.values()[0], 0.420000);
		condomUseIntentions.put(FemaleType.values()[1], 1.260000);
		condomUseIntentions.put(FemaleType.values()[2], 1.260000);
		condomUseIntentions.put(FemaleType.values()[3], 0.420000);
		condomUseIntentions.put(FemaleType.values()[4], 1.260000);
		condomUseIntentions.put(FemaleType.values()[5], 0.420000);
		condomUseIntentions.put(FemaleType.values()[6], 0.420000);
		condomUseIntentions.put(FemaleType.values()[7], 1.260000);
		condomUseIntentions.put(FemaleType.values()[8], 1.260000);
		condomUseIntentions.put(FemaleType.values()[9], 0.420000);

	}
	
	/*
	private void initialiseSexWillingnessForType7() {
		
		sexWillingness.put(FemaleType.values()[0], 0.613636);
		sexWillingness.put(FemaleType.values()[1], 0.204545);
		sexWillingness.put(FemaleType.values()[2], 0.409091);
		sexWillingness.put(FemaleType.values()[3], 0.613636);
		sexWillingness.put(FemaleType.values()[4], 0.613636);
		sexWillingness.put(FemaleType.values()[5], 0.613636);
		sexWillingness.put(FemaleType.values()[6], 0.613636);
		sexWillingness.put(FemaleType.values()[7], 0.204545);
		sexWillingness.put(FemaleType.values()[8], 0.409091);
		sexWillingness.put(FemaleType.values()[9], 0.204545);
	}
	
	private void initialiseCondomUseIntentionsForType7() {
		condomUseIntentions.put(FemaleType.values()[0], 0.420000);
		condomUseIntentions.put(FemaleType.values()[1], 1.260000);
		condomUseIntentions.put(FemaleType.values()[2], 1.260000);
		condomUseIntentions.put(FemaleType.values()[3], 0.420000);
		condomUseIntentions.put(FemaleType.values()[4], 1.260000);
		condomUseIntentions.put(FemaleType.values()[5], 0.420000);
		condomUseIntentions.put(FemaleType.values()[6], 0.420000);
		condomUseIntentions.put(FemaleType.values()[7], 1.260000);
		condomUseIntentions.put(FemaleType.values()[8], 1.260000);
		condomUseIntentions.put(FemaleType.values()[9], 0.420000);

	}*/


	private void initialiseSexWillingnessForType8() {
		sexWillingness.put(FemaleType.values()[0], 0.250000);
		sexWillingness.put(FemaleType.values()[1], 0.500000);
		sexWillingness.put(FemaleType.values()[2], 0.500000);
		sexWillingness.put(FemaleType.values()[3], 0.750000);
		sexWillingness.put(FemaleType.values()[4], 0.250000);
		sexWillingness.put(FemaleType.values()[5], 0.250000);
		sexWillingness.put(FemaleType.values()[6], 0.500000);
		sexWillingness.put(FemaleType.values()[7], 0.250000);
		sexWillingness.put(FemaleType.values()[8], 0.500000);
		sexWillingness.put(FemaleType.values()[9], 0.750000);

		
	}
	
	private void initialiseCondomUseIntentionsForType8() {
		condomUseIntentions.put(FemaleType.values()[0], 0.466667);
		condomUseIntentions.put(FemaleType.values()[1], 0.466667);
		condomUseIntentions.put(FemaleType.values()[2], 0.466667);
		condomUseIntentions.put(FemaleType.values()[3], 1.400000);
		condomUseIntentions.put(FemaleType.values()[4], 0.933333);
		condomUseIntentions.put(FemaleType.values()[5], 1.400000);
		condomUseIntentions.put(FemaleType.values()[6], 0.933333);
		condomUseIntentions.put(FemaleType.values()[7], 0.933333);
		condomUseIntentions.put(FemaleType.values()[8], 0.933333);
		condomUseIntentions.put(FemaleType.values()[9], 0.466667);
	}
	
	
	private void initialiseSexWillingnessForType9() {
		sexWillingness.put(FemaleType.values()[0], 0.409091);
		sexWillingness.put(FemaleType.values()[1], 0.204545);
		sexWillingness.put(FemaleType.values()[2], 0.613636);
		sexWillingness.put(FemaleType.values()[3], 0.613636);
		sexWillingness.put(FemaleType.values()[4], 0.613636);
		sexWillingness.put(FemaleType.values()[5], 0.409091);
		sexWillingness.put(FemaleType.values()[6], 0.613636);
		sexWillingness.put(FemaleType.values()[7], 0.409091);
		sexWillingness.put(FemaleType.values()[8], 0.204545);
		sexWillingness.put(FemaleType.values()[9], 0.409091);
	
	}
	
	private void initialiseCondomUseIntentionsForType9() {
		condomUseIntentions.put(FemaleType.values()[0], 0.868966);
		condomUseIntentions.put(FemaleType.values()[1], 0.868966);
		condomUseIntentions.put(FemaleType.values()[2], 0.868966);
		condomUseIntentions.put(FemaleType.values()[3], 0.868966);
		condomUseIntentions.put(FemaleType.values()[4], 0.868966);
		condomUseIntentions.put(FemaleType.values()[5], 0.868966);
		condomUseIntentions.put(FemaleType.values()[6], 0.868966);
		condomUseIntentions.put(FemaleType.values()[7], 0.579310);
		condomUseIntentions.put(FemaleType.values()[8], 0.868966);
		condomUseIntentions.put(FemaleType.values()[9], 0.868966);
	}
	

	public void incrementNumOfSexualEncounters() {
		numberOfSexualEncounters++;
	}

	public void incrementNumOfSexualEncountersWithCondom() {
		numOfSexualEncountersWithCondom++;
	}


	public String toString(){
		
		String s = type + " had sex with " + numberOfSexualEncounters + " women and used condom with " + numOfSexualEncountersWithCondom;
		if (disease)
			s += " and he got a disease from " + heGotTheDiseaseFrom;
		else
			s += " 			                                  				      ";
		
		return s;
	}


	public ImageIcon getImage() {
		return image;
	}

	public void incrementNumOfDiseases() {
		numOfDiseases++;
	}

	public String toStringForMultipleSimulations(){
	
		String s = type + " (" + clones + " clones) had sex with " + numberOfSexualEncounters/clones + " women and used condom with " + numOfSexualEncountersWithCondom/clones + " and he got infected by " + heGotTheDiseaseFrom.size() + " types and he gave the infection to " + heGaveTheDiseaseTo.size() + " types.";		                                  				      
		printed = true;
		return s;
	}
	
	
}
