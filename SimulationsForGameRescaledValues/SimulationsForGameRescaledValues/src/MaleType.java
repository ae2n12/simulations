
public enum MaleType {
	TypeA, TypeB, TypeC, TypeD, TypeE, TypeF, TypeG, TypeH, TypeI;
	
	public int compare(MaleType a, MaleType b){
		if (a.ordinal()<b.ordinal())
			return -1;
		if (b.ordinal()<a.ordinal()){
			return 1;
		}
		else
			return 0;
	}
}
