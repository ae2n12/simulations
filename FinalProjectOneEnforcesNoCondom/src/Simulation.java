import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Simulation {
	
	static PrintWriter writer; 
	static ArrayList<Person> people;

	public static void main(String[] args) throws FileNotFoundException {
		 writer = new PrintWriter(new File("results.txt"));
		 
		 //Create population
		 people = new ArrayList<Person>();
		 for(char c = 'A'; c<='Z'; c++){
			 people.add(new Person(c, c));
		 }
		 
		 writer.println("People initially infected");
		 //Randomly assign infections
		 Random rand = new Random();
		 for (int i=0; i<3; i++){
			 int n = rand.nextInt(26);
			 people.get(n).infected = true;
			 writer.print(people.get(n).name + ", ");
		 }
		 
		 writer.println();
		 
		 //Mating
		 ArrayList<Person> copyOfPeople = new ArrayList<Person>();
		 copyOfPeople.addAll(people);
		 
		 writer.println("***Mating***");
		 for (int rep=0; rep<7; rep++){ //Repeat the mating 5 times
			 Collections.shuffle(copyOfPeople); 
			 for (int i=0; i<copyOfPeople.size()-1; i+=2){
				 Encounter e = new Encounter(copyOfPeople.get(i), copyOfPeople.get(i+1));
				 writer.println(e.toString());
			 }
		 }
		 
		 writer.println();
		 writer.println("***Summary***");
		 for (int i=0; i<people.size(); i++){
			 writer.println(people.get(i).toString());
		 }
		 
		 writer.close();

	}

}
