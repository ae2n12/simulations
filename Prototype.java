import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.*;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.*;

import vtk.vtkDoubleArray;
import vtk.vtkGraphLayoutView;
import vtk.vtkIntArray;
import vtk.vtkLookupTable;
import vtk.vtkMutableUndirectedGraph;
import vtk.vtkNativeLibrary;
import vtk.vtkUnsignedCharArray;
import vtk.vtkVertexListIterator;
import vtk.vtkViewTheme;


public class Prototype extends JFrame implements ActionListener {
	
	private Panel simulationPanel;	// Display Panel	
	private JPanel summaryPanel;
	private JPanel motherPanel;
	private final static String MOTHERPANEL = "MotherPanel"; 
	private final static String SUMMARYPANEL = "SummaryPanel";
	private CardLayout cardLayout;
	private Runner runner;							// Runner for thread
	private Thread thread;							// Thread for people
	private JButton random;							// New deposits button
	private JButton start;							// Start button
	private Person[] people;
	PrintWriter writer;
	int aids;

	/* Load VTK shared librarires (.dll) on startup, print message if not found */
	static {
		if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				if (!lib.IsLoaded())
					System.out.println(lib.GetLibraryName() + " not loaded");
			}

			System.out.println("Make sure the search path is correct: ");
			System.out.println(System.getProperty("java.library.path"));
		}
		vtkNativeLibrary.DisableOutputWindow(null);
	}
	
	public Prototype() {
		
		// Add windon listener for closing
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				dispose();
				System.exit(0);
			}
		});
		
		cardLayout = new CardLayout();
		this.getContentPane().setLayout(cardLayout);		
		
		String fileName = (new Date()).getTime() + "SH";
		
		try {
			writer = new PrintWriter(fileName+".txt", "UTF-8");
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		writer.println("			SIMULATION RESULTS");
		writer.println("			Participant's code: " + fileName);
		writer.println();
		writer.println();
		writer.println();
		
		start = new JButton("Start");
		start.addActionListener(this);
		
		
		JPanel controlPanel = new JPanel();
		
		controlPanel.setLayout( new GridLayout(12,1) );
		JLabel blue = new JLabel("YOU are the big dot.");
		blue.setForeground(Color.BLUE);
		controlPanel.add(blue);
		JLabel gray = new JLabel("HEALTHY PEOPLE are the gray.");
		gray.setForeground(Color.GRAY);
		controlPanel.add(gray);
		JLabel gray3 = new JLabel("Very attractive people are ovals.");
		gray.setForeground(Color.GRAY);
		controlPanel.add(gray3);
		JLabel red = new JLabel ("AIDS");
		red.setForeground(Color.RED);
		controlPanel.add(red);
		JLabel herpes = new JLabel ("HERPES");
		herpes.setForeground(Color.GREEN);
		controlPanel.add(herpes);
		JLabel chlamydia = new JLabel ("CHLAMYDIA");
		chlamydia.setForeground(Color.ORANGE);
		controlPanel.add(chlamydia);
		JLabel gray2 = new JLabel("MORE THAN ONE DISEASE");
		gray.setForeground(Color.BLACK);
		controlPanel.add(gray2);
		controlPanel.add(start);
		final JButton stop = new JButton("Stop");
		controlPanel.add(stop);
		final JButton playAgain = new JButton("Play again");
		controlPanel.add(playAgain);
		playAgain.setEnabled(false);
		final JButton finish = new JButton("Finish");
		controlPanel.add(finish);
		finish.setEnabled(false);
		
		// Add controls and display to the main frame
		motherPanel = new JPanel();
		
		motherPanel.setLayout( new BorderLayout() );
		motherPanel.add("East", controlPanel);
		
		getContentPane().add(motherPanel, MOTHERPANEL);
	
		initialiseSimulation();
		
		cardLayout.show(getContentPane(), MOTHERPANEL);
		
		//Create summary panel
				summaryPanel = new JPanel();
				ModifiedFlowLayout layout1 = new ModifiedFlowLayout();
				summaryPanel.setLayout(layout1);
				
				JLabel summary = new JLabel("SIMULATION COMPLETED SUCCESSFULLY!");
				summary.setFont(new Font("Serif", Font.BOLD, 28));
				
				summaryPanel.add(summary);	
				JLabel file = new JLabel("You can read the results in the detailed log file produced.");
				file.setFont(new Font("Serif", Font.BOLD, 18));
				summaryPanel.add(file);
				JLabel thanks = new JLabel("Thank you very much for your time.");
				thanks.setFont(new Font("Serif", Font.BOLD, 18));
				summaryPanel.add(thanks);
				
				BufferedImage img = null ;
				try{
					img = ImageIO.read(new File("src/logo.jpg"));
				}catch (IOException e){}
				
				Image dimg = img.getScaledInstance(250, 150, Image.SCALE_FAST);
				JLabel pic = new JLabel(new ImageIcon(dimg));
				
				summaryPanel.add(pic);
				
				BufferedImage img2 = null ;				
				
				try{
					img2 = ImageIO.read(new File("src/EPSRC_logo.jpg"));
				}catch (IOException e){}
				
				Image dimg2 = img2.getScaledInstance(250, 150, Image.SCALE_FAST);
				JLabel pic2 = new JLabel(new ImageIcon(dimg2));
				
				summaryPanel.add(pic2);
				

				JScrollPane scrollPane1 = new JScrollPane(summaryPanel);
				
			    scrollPane1.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			    scrollPane1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			    scrollPane1.createVerticalScrollBar();
			    scrollPane1.setBounds(0, 0, 610, 930);	
				scrollPane1.setViewportView(summaryPanel);

				getContentPane().add(scrollPane1, SUMMARYPANEL);

		
		stop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				people = simulationPanel.getPeople();
				simulationPanel.setRunning(false);
				runner.stop = true;
				thread.stop();
				stop.setEnabled(false);
				playAgain.setEnabled(true);
				finish.setEnabled(true);				
				aids = countPeopleWithAids(people);
				writer.println("The user caught an STD at " + (simulationPanel.getTimeUntilSTD())/1000 + " seconds.");
			
			}
			
		});
		
		playAgain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				stop.setEnabled(true);
				playAgain.setEnabled(false);
				finish.setEnabled(false);
				simulationPanel.setRunning(true);
				simulationPanel.resetClockLabel();
				runner = new Runner();
				thread = new Thread(runner);
				thread.setPriority( thread.getPriority() - 1 );
				thread.start();  
				start.setEnabled(false);
				random.setEnabled(false);
				simulationPanel.setTimeUntilSTD(System.currentTimeMillis());
			}
			
		});
		
		finish.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				String str = "Do you want to exit the simulation? If yes, your results will be saved in a log file" + "\n" + "and "
						+ "you won't be able to return to this screen. If you wish to play again, click No";
				int n = JOptionPane.showConfirmDialog(
                        motherPanel, str,
                        "Exit",
                        JOptionPane.YES_NO_OPTION);
                if (n == JOptionPane.YES_OPTION) {
                	cardLayout.show(getContentPane(), SUMMARYPANEL);
                	displayContactsGraph();
                	displayAllContactsGraph();
                } else 
                 {
                    //Do nothing
                }
			}
			
		});
		
	}


	private void initialiseSimulation() {
		int maxAttractiveness = 4;
		boolean[][] risk = new boolean[100][100]; //how risky men will be with the women there
		risk = initialiseRisk(risk);
		int[] attractiveness = new int[100];
		attractiveness = initialiseAttractiveness(attractiveness);
		simulationPanel = new Panel(100, maxAttractiveness, risk, attractiveness);
		motherPanel.add("Center",simulationPanel);
		String str = "Thank you for your answers." + "\n" + "\n" 
					+ "Based on your answers, a simulated world of 100 men and 100 women will launch now to show you "
				    + "\n" + "what can happen in practice if the risk is as you described." + "\n" + "\n"
				     + "Gray dots are healthy people and red dots are people with sexually transmitted diseases." + "\n" +
				      "When people meet there is a chance of having unprotected sex. When one of them is red already, \n there is a "
				      + "chance that the gray dot will become red (diseased), too." + "\n" + "\n" 
				      + "The squares represent not attractive people. Red squares are diseased (same rules apply for diseases)." + "\n" + "\n" +
					 "You will be the blue dot (you are slightly bigger than the rest)."
		             + "\n" + "You only need to start the simulation and watch what will happen." + "\n" +
					 "You are expected to inform the researcher before quiting. You are supposed to have three runs." + "\n" +
					 "Let's see if you will manage to stay healthy (i.e stay blue) until the end." ;
		JOptionPane.showMessageDialog(null, str);
	}


	private int[] initialiseAttractiveness(int[] attractiveness) {
		//returns an array with women attractiveness rates
		for (int i=0; i<attractiveness.length;i++){
			attractiveness[i] = 7;
		}
		for (int i=0; i<attractiveness.length/2;i++){
			attractiveness[i] = 2;
		}
		return attractiveness;
	}


	private boolean[][] initialiseRisk(boolean[][] risk) {
	//Each row shows the risk of each man with each woman.
    //This method initialises the RISK
		for (int i=0; i<risk.length; i++){
			for (int j=0; j<risk.length;j++){
				risk[i][j] = true;
			}
		}
		
		return risk;
	}


	private int countPeopleWithAids(Person[] people) {
		int count = 0;
		for (int i=0; i<people.length;i++){
			if (people[i].getColor() == Color.red){
				count++;
			}
		}
		return count;
	}

	public static void main(String args[]) {
		System.out.println("Starting simulation...");
		Prototype mainFrame = new Prototype();
		mainFrame.setSize(810, 450);
		mainFrame.move(100,100);
		mainFrame.setTitle("Simulated world");
		mainFrame.setVisible(true);
		mainFrame.setResizable(false);
	}
	
	// Listener for controls.
	
	public void actionPerformed( ActionEvent e ) {
		
		// Create the thread and update the controls.
		if ( e.getSource() == start ) {
			simulationPanel.resetClockLabel();
			simulationPanel.setRunning(true);
			runner = new Runner();
			thread = new Thread(runner);
			thread.setPriority( thread.getPriority() - 1 );
			thread.start();
			start.setEnabled(false);
		}

	}
	
	void displayAllContactsGraph(){
		vtkMutableUndirectedGraph g = new vtkMutableUndirectedGraph();
		HashMap<Integer, Integer> mapping = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> imapping = new HashMap<Integer, Integer>();
		
		vtkDoubleArray vertexColors = new vtkDoubleArray();
		vertexColors.SetNumberOfComponents(3);
		vertexColors.SetName("customColor");
		
		double red[] = {1.0, 0.0, 0.0};
		double green[] = {0.0, 1.0, 0.0};
		double blue[] = {0.0, 0.0, 1.0};
		double yellow[] = {1.0, 1.0, 0.0};
		double black[] = {0.0, 0.0, 0.0};

		for(Person p : people){
			if(p.getColor() != Color.lightGray && p.getColor() != Color.BLUE){
				int v = g.AddVertex();
				mapping.put(v, p.getID());
				imapping.put(p.getID(), v);
			}
		}

		vtkVertexListIterator it = new vtkVertexListIterator();
		g.GetVertices(it);
		
		while(it.HasNext()){
			int v = it.Next();
			Person p = people[mapping.get(v)];
			
			double color[];
			if(p.getColor() == Color.RED)
				color = red;
			else if(p.getColor() == Color.GREEN)
				color = green;
			else if(p.getColor() == Color.BLUE)
				color = blue;
			else if(p.getColor() == Color.YELLOW)
				color = yellow;
			else
				color = black;

			vertexColors.InsertNextTuple3(color[0], color[1], color[2]);
			
			for(Person k : p.listOfAllContacts){
				if (imapping.get(k.getID()) != null)
					g.AddGraphEdge(v, imapping.get(k.getID()));
			}
		}
		
		g.GetVertexData().AddArray(vertexColors);
		
		vtkGraphLayoutView view = new vtkGraphLayoutView();
		view.AddRepresentationFromInput(g);
		view.SetLayoutStrategy("Simple 2D");
		view.SetVertexColorArrayName("customColor");
		view.ColorVerticesOn();

		view.GetRenderer().SetBackground(1.0, 1.0, 1.0);
		view.ResetCamera();
		view.Render();
		view.GetInteractor().Initialize();
		view.GetInteractor().Start();
	}
	
	void displayContactsGraph(){
		vtkMutableUndirectedGraph g = new vtkMutableUndirectedGraph();
		HashMap<Integer, Integer> mapping = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> imapping = new HashMap<Integer, Integer>();
		
		vtkDoubleArray vertexColors = new vtkDoubleArray();
		vertexColors.SetNumberOfComponents(3);
		vertexColors.SetName("customColor");
		
		double red[] = {1.0, 0.0, 0.0};
		double green[] = {0.0, 1.0, 0.0};
		double blue[] = {0.0, 0.0, 1.0};
		double yellow[] = {1.0, 1.0, 0.0};
		double black[] = {0.0, 0.0, 0.0};

		for(Person p : people){
			if(p.getColor() != Color.lightGray && p.getColor() != Color.BLUE){
				int v = g.AddVertex();
				mapping.put(v, p.getID());
				imapping.put(p.getID(), v);
			}
		}

		vtkVertexListIterator it = new vtkVertexListIterator();
		g.GetVertices(it);
		
		while(it.HasNext()){
			int v = it.Next();
			Person p = people[mapping.get(v)];
			
			double color[];
			if(p.getColor() == Color.RED)
				color = red;
			else if(p.getColor() == Color.GREEN)
				color = green;
			else if(p.getColor() == Color.BLUE)
				color = blue;
			else if(p.getColor() == Color.YELLOW)
				color = yellow;
			else
				color = black;

			vertexColors.InsertNextTuple3(color[0], color[1], color[2]);
			
			for(Person k : p.listOfContacts){
				if (imapping.get(k.getID()) != null)
					g.AddGraphEdge(v, imapping.get(k.getID()));
			}
		}
		
		g.GetVertexData().AddArray(vertexColors);
		
		vtkGraphLayoutView view = new vtkGraphLayoutView();
		view.AddRepresentationFromInput(g);
		view.SetLayoutStrategy("Simple 2D");
		view.SetVertexColorArrayName("customColor");
		view.ColorVerticesOn();

		view.GetRenderer().SetBackground(1.0, 1.0, 1.0);
		view.ResetCamera();
		view.Render();
		view.GetInteractor().Initialize();
		view.GetInteractor().Start();
	}
	
	
	// Private class in which the simulation actually runs.
	
	private class Runner implements Runnable {
		
		private boolean stop;
		// Constructor constants.
		public Runner() {
			stop = false;
		}
				
		// Method to start the thread running.
		public void run() {
			
			// Continue until stop is pressed.
			while ( !stop ) {
				simulationPanel.step(true );
				try {
					thread.sleep( (50) );
				} catch ( Exception e ) {
				}					
			}		
		}
	}	
	
}
